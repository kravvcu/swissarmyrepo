#!/bin/bash

set -e

# sudo apt update
# sudo apt upgrade --yes

# install brew and install most of the tools using brew rather than manually
# additionally asdf is also nice

# when fails run dpkg --configure -a
# when that fails run: apt install -f
# rerun upgrade
sudo apt install -y python3-pip \
                    python3-dev \
                    python-is-python3 \
                    git \
                    maven \
                    colordiff \
                    at \
                    htop \
                    screen \
                    unzip \
                    xclip \
                    terminator \
                    whois

sudo pip3 install -U pip
sudo pip3 install setuptools
sudo pip3 install virtualenv virtualenvwrapper
sudo pip3 install psutil


brew install fd
#export WORKON_HOME=$HOME/.virtualenvs
#export PROJECT_HOME=$HOME/Devel
#if type virtualenvwrapper.sh > /dev/null; then
#  source /usr/local/bin/virtualenvwrapper.sh
#fi

# ADJUST ALL TO AN UBUNTU / DEBIAN installation

# install tfenv: https://github.com/tfutils/tfenv
# install tgenv: https://github.com/cunymatthieu/tgenv

# zsh: ALT+F shorcut should move to end of the word not to the beginning of the next one
#
# mkvirtualenv ansible
# pip install ansible

# install go - untar to /usr/local
# export GOPATH="$HOME/.go"
# export PATH="$PATH:/usr/local/go/bin:$HOME/go"

# change gitconfig ansible so that even when user / email not provided, ansible wont change
# the current configuration

# install java from oracle or openjdk

# run ansible: shell.yml, provision.yml

# update knowledgebase from backup
# zsh change prompt to view kubectl context
# install gdebip
# install signal + add to startup + hide to tray
# install discord + add to startup
# install GUI apps only when GUI is available
# install fzf
# install flameshot

# install awscli https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html
# install groff, glibc and less

# add AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY to .zprofile

# install `markdown-preview-mode` in emacs
# install `markdown` for the emacs markdown preview mode


# backup with tar
# tar -zcvpf /media/xyz/home.tgz -X tar-excludes-file /home/xyz

# install common lisp
# install sbcl binary from http://www.sbcl.org/platform-table.html
# install quicklisp https://lispcookbook.github.io/cl-cookbook/getting-started.html
