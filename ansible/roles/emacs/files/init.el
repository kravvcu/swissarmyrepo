;; add package archives
(require 'package)

;; as of this writing (emacs 28.1) gnu and non-gnu are added by default
;; but add them explicitly anyways

(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/") t)
(add-to-list 'package-archives '("melpa-stable" . "https://melpa.org/packages/") t)

(package-initialize)

(org-babel-load-file "~/.emacs.d/configuration.org")
(load-file "~/.emacs.d/emacs.old.el")
