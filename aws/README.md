# AWS

## Prerequisites

### Reaching AWS API

Log into AWS and generate an access key and secret access key. Configure the AWS CLI:

```bash
  $ aws configure
  # pass in the access key ID and secret key values when prompted
```

That's it. You can start using AWS through Terraform now.

## Certified Solutions Architect

This part of the repository contains Jsonnet code to setup AWS infrastructure for different lab
scenarios for learning purposes.

1. [VM in default VPC](./envs/vm-in-default-vpc/README.md)

    Create and SSH into a VM in the default VPC using different methods.

1. [VM in custom VPC](./envs/vm-in-custom-vpc/README.md)

    Learn what it takes to setup a custom VPC, subnet, a VM in that subnet and what other resources
    are needed to be able to SSH into the VM.

1. [Security groups vs NACLs](./envs/security-groups-vs-nacls/README.md)

    Learn what are the differences between Security Groups and Network Access Control Lists and how
    the two work together.

1. [Bastion Host & NAT Gateway](./envs/bastion-host-nat-gateway/README.md)

    Create a private EC2 Instance and connect to it using a Bastion Host. Provide Internet access
    to the private EC2 Instance.

1. [VPC Endpoints](./envs/vpc-endpoints/README.md)

   Provide access to the S3 service to an EC2 Instance deployed in a private subnet (without
   Internet access).

1. [IAM Basics](./envs/iam-basics/README.md)

   Create an IAM user and explore the different ways of assigning them S3 bucket list permissions.
