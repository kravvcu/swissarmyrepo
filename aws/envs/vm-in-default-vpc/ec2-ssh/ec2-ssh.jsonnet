local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

local instance = import "ec2_instance.libsonnet";
local security_group = import "security_group.libsonnet";

{
  cidr_blocks:: 'Replace with own IP in CIDR format to permit SSH',
  resource_name_prefix:: 'vm-in-default-vpc-ssh',

  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/ec2-ssh"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'ec2-ssh.tf.json': {
    data: {
      aws_ami: {
        ubuntu: instance.AWSAMI
      }
    },

    resource: {
      aws_key_pair: {
        kp: {
          key_name_prefix: $.resource_name_prefix,
          public_key: '${file("~/.ssh/id_rsa.pub")}',
        },
      },
      aws_security_group: {
        permit_ssh: {
          name_prefix: $.resource_name_prefix,
          vpc_id: env.vpc_id,

          ingress: [
            security_group.SecurityGroupRule {
              description: "Permit inbound SSH traffic",
              from_port: "22",
              to_port: "22",
              protocol: "tcp",
              cidr_blocks: [$.cidr_blocks],
            }
          ],

          egress: [
            security_group.SecurityGroupRule {
              description: "Permit all outbound traffic",
              from_port: "0",
              to_port: "0",
              protocol: "-1",
              cidr_blocks: ["0.0.0.0/0"],
            }
          ]
        },
      },
      aws_instance: {
        ubuntu: instance.AWSInstance {
          name: "vm-in-default-vpc",
          ami: "${data.aws_ami.ubuntu.id}",
          vpc_security_group_ids: [
            "${aws_security_group.permit_ssh.id}",
          ],
          key_name: '${aws_key_pair.kp.key_name}',
        }
      }
    },
  }
}
