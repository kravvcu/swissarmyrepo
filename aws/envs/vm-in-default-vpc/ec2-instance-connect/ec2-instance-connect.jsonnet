local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

local instance = import "ec2_instance.libsonnet";
local security_group = import "security_group.libsonnet";

{
  ec2_instance_connect_cidr:: '3.8.37.24/29', # for eu-west2 http://ip-ranges.amazonaws.com/ip-ranges.json

  resource_name_prefix:: 'vm-in-default-vpc-instance-connect-',
  cidr_blocks:: 'Replace with own IP in CIDR format to permit SSH',

  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/ec2-instance-connect"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'ec2-instance-connect.tf.json': {
    data: {
      aws_ami: {
        ubuntu: instance.AWSAMI
      }
    },

    resource: {
      aws_security_group: {
        permit_ssh: {
          name_prefix: $.resource_name_prefix,
          vpc_id: env.vpc_id,

          ingress: [
            security_group.SecurityGroupRule {
              description: "Permit Instance Connect - CLI+SSH and browser",
              from_port: "22",
              to_port: "22",
              protocol: "tcp",
              cidr_blocks: [$.cidr_blocks, $.ec2_instance_connect_cidr],
            }
          ],

          egress: [
            security_group.SecurityGroupRule {
              description: "Permit all outbound traffic",
              from_port: "0",
              to_port: "0",
              protocol: "-1",
              cidr_blocks: ["0.0.0.0/0"],
            }
          ]
        },
      },
      aws_instance: {
        ubuntu: instance.AWSInstance {
          name: "vm-in-default-vpc",
          # ubuntu 20.04 or later already have instance connect installed
          # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-connect-set-up.html
          ami: "${data.aws_ami.ubuntu.id}",
          vpc_security_group_ids: [
            "${aws_security_group.permit_ssh.id}",
          ],
        }
      }
    },
  }
}
