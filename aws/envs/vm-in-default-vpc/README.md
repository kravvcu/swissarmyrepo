# VM in a default VPC

This section is about creation of VMs in the default VPC and connecting to them through different
mechanisms:

- Using a pre-provisioned, own SSH key - see [Exercises - SSH](#exercises-ssh).
- Using Instance Connect - see [Exercises - Instance Connect](#exercises-instance-connect).

## Documentation:

- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/connection-prereqs.html
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstances.html

## Exercises - SSH

1. SSH into the VM from your local CLI.
1. Install some software e.g. `vim`.
1. Ping the outside world e.g. `www.google.com`.

<details>
  <summary>Show solution</summary>
  <p>

  ```bash
  # fetch the IP of your instance (assuming you only have one) and run SSH
  # note that the user might differ depending on the OS

  $ INSTANCE_IP=$(aws ec2 describe-instances --query 'Reservations[*].Instances[*].{Name:PublicIpAddress}' --output text)
  $ ssh ubuntu@${INSTANCE_IP}

  $ sudo apt update && sudo apt install -y vim
  $ ping www.google.com
  ```

  </p>
</details>

## Exercises - Instance Connect

1. Connect to the VM using the browser.

    <details>
      <summary>Show solution</summary>
      <p>

      1. Log into the AWS console.
      1. Navigate to `EC2 -> Instances`.
      1. Select the VM and push the `Connect` button.
      1. Select `EC2 Instance Connect (browser-based SSH session)` and push the `Connect` button.

      </p>
    </details>

1. Connect to the VM using EC2 Instance Connect CLI.

    <details>
      <summary>Show solution</summary>
      <p>

      ```bash
      # you probably want to run this in a virtualenv
      $ pip install ec2instanceconnectcli
      $ INSTANCE_ID=$(aws ec2 describe-instances --query 'Reservations[*].Instances[*].{Name:InstanceId}' --output text)

      # the user will differ depending on the VM OS
      $ mssh ubuntu@${INSTANCE_ID}
      ```

      </p>
    </details>

1. Connect to the VM using your own SSH key and client.

    <details>
      <summary>Show solution</summary>
      <p>

      ```bash
      $ INSTANCE_ID=$(aws ec2 describe-instances --query 'Reservations[*].Instances[*].{Name:InstanceId}' --output text)
      $ INSTANCE_AZ=$(aws ec2 describe-instances --query 'Reservations[*].Instances[*].{Name:Placement.AvailabilityZone}' --output text)
      $ INSTANCE_IP=$(aws ec2 describe-instances --query 'Reservations[*].Instances[*].{Name:PublicIpAddress}' --output text)

      $ aws ec2-instance-connect send-ssh-public-key \
          --instance-id ${INSTANCE_ID} \
          --availability-zone ${INSTANCE_AZ} \
          --instance-os-user ubuntu \
          --ssh-public-key file://~/.ssh/id_rsa.pub

      $ ssh ubuntu@${INSTANCE_IP}
      ```

      </p>
    </details>

## Review

- Instance Connect and/or SSH access needs the following to be configured:

  - An SSH key pair.
  - A security group permitting inbound connections to the `22` port.
  - An instance configured with the abovementions SSH key pair and security group.

- If access to external services is needed from the VM, the egress rule needs to allow it.

- When using Instance Connect through the AWS console (browser), the security group needs to allow
  connections from the appropriate [AWS region CIDR](http://ip-ranges.amazonaws.com/ip-ranges.json).
