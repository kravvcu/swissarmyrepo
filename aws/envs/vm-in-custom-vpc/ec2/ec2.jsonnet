local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

local instance = import "ec2_instance.libsonnet";
local security_group = import "security_group.libsonnet";

{
  cidr_blocks:: 'Replace with own IP in CIDR format to permit SSH',

  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/ec2"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'data_sources.tf.json': {
    data: {
      aws_ami: {
        ubuntu: instance.AWSAMI
      },
      terraform_remote_state: {
        vpc: env.EnvTerraformS3RemoteState {
          key_suffix:: '/vpc'
        },
      },
    },
  },

  'ec2.tf.json': {
    resource: {
      aws_key_pair: {
        kp: {
          key_name_prefix: env.resource_name_prefix,
          public_key: '${file("~/.ssh/id_rsa.pub")}',
        },
      },
      aws_security_group: {
        permit_ssh: {
          name_prefix: env.resource_name_prefix + "-sg",
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",

          ingress: [
            security_group.SecurityGroupRule {
              description: "Permit inbound SSH traffic",
              from_port: "22",
              to_port: "22",
              protocol: "tcp",
              cidr_blocks: [$.cidr_blocks],
            }
          ],

          egress: [
            security_group.SecurityGroupRule {
              description: "Permit all outbound traffic",
              from_port: "0",
              to_port: "0",
              protocol: "-1",
              cidr_blocks: ["0.0.0.0/0"],
            }
          ]
        },
      },
      aws_instance: {
        ubuntu: instance.AWSInstance {
          name: env.resource_name_prefix,
          ami: "${data.aws_ami.ubuntu.id}",
          key_name: '${aws_key_pair.kp.key_name}',
          subnet_id: "${data.terraform_remote_state.vpc.outputs.subnet_id}",
          vpc_security_group_ids: [
            "${aws_security_group.permit_ssh.id}"
          ],
          # associate_public_ip_address: true,
        }
      }
    }
  }
}
