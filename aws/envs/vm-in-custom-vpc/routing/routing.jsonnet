local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

local igw = import "igw.libsonnet";
local route = import "route.libsonnet";
local route_table = import "route_table.libsonnet";
local route_table_association = import "route_table_association.libsonnet";

{
  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/routing"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'data_sources.tf.json': {
    data: {
      terraform_remote_state: {
        vpc: env.EnvTerraformS3RemoteState {
          key_suffix:: '/vpc'
        },
      },
    },
  },

  'routing.tf.json': {
    resource: {
      aws_internet_gateway: {
        igw: igw.InternetGateway {
          name:: env.resource_name_prefix + "-igw",
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",
        }
      },
      aws_route_table: {
        rt: route_table.RouteTable {
          name:: env.resource_name_prefix + "-rt",
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",
          route: [
            route.Route {
              cidr_block: "0.0.0.0/0",
              gateway_id: "${aws_internet_gateway.igw.id}"
            }
          ]
        }
      },
      aws_route_table_association: {
        rta: route_table_association.RouteTableAssociation {
          route_table_id: "${aws_route_table.rt.id}",
          subnet_id: "${data.terraform_remote_state.vpc.outputs.subnet_id}"
        }
      },
    }
  },
}
