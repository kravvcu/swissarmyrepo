# VM in a custom VPC

This section is about creation of a:

- VPC and a subnet
- VM in that subnet
- various other resources needed to be able to connect to the VM with SSH

## Documentation:

- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-instance-addressing.html#public-ip-addresses
- https://docs.aws.amazon.com/vpc/latest/userguide/vpc-subnets-commands-example.html

## Exercises

1. Create the VPC by running Terraform in the `vpc` directory.
1. Create the VM by running Terraform in the `ec2` directory.

  - Adjust the `cidr_block` value in `ec2/ec2.jsonnet`.
  - Regenerate the Terraform resource definitions and run Terraform:

    ```bash
       $ jsonnet -J ../../../lib --multi . ec2.jsonnet
       $ teraform apply
    ```

1. The VM is created without a public IP address. Why?

    <details>
      <summary>Show answer</summary>
      <p>

      Only the default subnets will automatically assign public IP addresses to EC2 instance
      interfaces. Custom subnets have this feature disabled by default. To assign a public IP to
      the instance we can either:

      - enable the feature on the subnet
      - override the setting for the instance

      Uncomment the `associate_public_ip_address` in the `ec2/ec2.jsonnet` file, run Jsonnet and
      Terraform to recreate the VM with a public IP address:

      ```bash
         $ jsonnet -J ../../../lib --multi . ec2.jsonnet
         $ terraform apply
      ```

      </p>
    </details>

1. The VM is created without a public DNS name. Why?

    <details>
      <summary>Show answer</summary>
      <p>

      Only default VPCs will automatically assign public DNS names to EC2 instances. Custom VPCs
      have this feature disabled. To assign a public DNS name to the instance, uncomment the
      `enable_dns_hostnames` setting in the `vpc/vpc.jsonnet` file, run Jsonnet and Terraform to
      enable public DNS assigments:

      ```bash
        $ jsonnet -J ../../../lib --multi . vpc.jsonnet
        $ terraform apply
      ```

      </p>
    </details>

1. The VM is not responding to any requests. It's not possible to SSH into it. Why?

    <details>
      <summary>Show answer</summary>
      <p>

      The subnet, which the VM has an interface in, lacks both an [Internet Gateway][igw] and a
      default route targeting that gateway in the default route table. Only default VPCs already
      have an Internet Gateway and a default route table with a default route targeting that
      gateway provisioned. Default subnets in the default VPC and custom subnets created in the
      default VPC have a default route already configured, which is why connecting to the VM in
      case of the [vm-in-default-vpc](../vm-in-default-vpc) scenario was possible.

      To get this [working](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-subnets-commands-example.html)
      in our setup, we need to create an Internet Gateway and a custom route table targeting that
      gateway for 0.0.0.0/0 (default route) traffic. Then, the route table needs to be associated
      either as a main routing table for the VPC or as a routing table for the subnet the VM
      resides in.

      ```bash
        $ cd routing
        $ jsonnet -J ./../../lib --multi . routing.jsonnet
        $ terraform apply
      ```

      </p>
    </details>

## Review

- For an EC2 Instance to have a public IP address assigned in a custom subnet:

  - The subnet needs to have the feature enabled

   or

  - The instance definition during creation needs to override the subnet setting.

- For an EC2 Instance to have a public DNS hostname assigned in a custom VPC, the VPC needs to have
  the `enable hostnames` feature enabled.

- For an EC2 instance to be able to communicate with the outside world, the subnet that the
  instance has an interface in needs to have a route table associated with it which has a
  default route targeting an [Internet Gateway][igw].

[igw]: https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html
