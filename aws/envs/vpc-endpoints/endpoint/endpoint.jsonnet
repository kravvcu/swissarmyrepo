local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

{
  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/endpoint"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'data_sources.tf.json': {
    data: {
      terraform_remote_state: {
        vpc: env.EnvTerraformS3RemoteState {
          key_suffix:: '/vpc'
        },
        routing: env.EnvTerraformS3RemoteState {
          key_suffix:: '/routing'
        },
      },
    },
  },

  'endpoint.tf.json': {
    resource: {
      aws_vpc_endpoint: {
        s3: {
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",
          service_name: std.format("com.amazonaws.%s.s3", env.region),
          route_table_ids: [
            "${data.terraform_remote_state.routing.outputs.private_route_table_id}"
          ]
        }
      }
    }
  },
}
