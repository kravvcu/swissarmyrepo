local provider = import "aws_provider.libsonnet";

local iam = import "iam.libsonnet";
local env = import "../env.jsonnet";

local iam_role = {
  assume_role_policy: {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Action": "sts:AssumeRole",
      }
    ]
  }
};

local iam_role_policy = {
  policy: {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": ["s3:*"],
        "Resource": ["*"]
      }
    ]
  }
};

{
  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/iam"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'iam.tf.json': {
    resource: {
      aws_iam_role: {
        ec2_s3_access: iam.IAMRole {
          name_prefix: env.resource_name_prefix,
          assume_role_policy: std.manifestJson(iam_role.assume_role_policy),
        },
      },
      aws_iam_role_policy: {
        s3_full_permissions: iam.IAMRolePolicy {
          name_prefix: env.resource_name_prefix,
          role: "${aws_iam_role.ec2_s3_access.id}",
          policy: std.manifestJson(iam_role_policy.policy),
        }
      },
      aws_iam_instance_profile: {
        s3_access_profile: {
          name_prefix: env.resource_name_prefix,
          role: "${aws_iam_role.ec2_s3_access.id}",
        }
      },
    },
  },

  'outputs.tf.json': {
    output: {
      s3_access_profile: {
        value: "${aws_iam_instance_profile.s3_access_profile.id}",
      }
    }
  }
}
