# VPC Endpoints

This scenario builds upon the [Bastion Host & NAT Gateway](./../bastion-host-nat-gateway/README.md)
scenario (omitting the NAT Gateway as it's not needed). We again setup a private EC2 Instance
and a Bastion Host and configure VPC Endpoints to provide the private instance access to
AWS S3 and DynamoDB services, without Internet access.

## Documentation:

- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/iam-roles-for-amazon-ec2.html#attach-iam-role
- https://docs.aws.amazon.com/vpc/latest/userguide/vpc-endpoints.html
- https://docs.aws.amazon.com/vpc/latest/userguide/vpce-gateway.html
- https://docs.aws.amazon.com/vpc/latest/userguide/vpce-interface.html
- https://aws.amazon.com/premiumsupport/knowledge-center/connect-s3-vpc-endpoint/

## Exercises

1. Create a VPC by running Terraform in the `vpc` directory.
1. Configure routing by running Terraform in the `routing` directory.
1. Create the S3 bucket by running Terraform in the `s3` directory.

  ```bash
    $ TF_VAR_bucket_name=your-bucket-name-123456 terraform apply
  ```

1. Create the EC2 Instances by running Terraform in the `ec2` directory.
1. SSH to the `private` EC2 Instance using the bastion host.

    <details>
      <summary>Show answer</summary>
      <p>

      ``` bash
        PRIVATE_INSTANCE_IP=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=vpc-endpoints-private-amazon-linux" --query 'Reservations[*].Instances[*].{Name:PrivateIpAddress}' --output text)
        BASTION_PUBLIC_DNS=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=vpc-endpoints-bastion" --query 'Reservations[*].Instances[*].{Name:PublicDnsName}' --output text)
        ssh -A -J ec2-user@${BASTION_PUBLIC_DNS} ec2-user@${PRIVATE_INSTANCE_IP}
      ```

      </p>
    </details>

1. Try to access S3. Notice that credentials will be needed.

   ```bash
     $ aws s3 ls
     Unable to locate credentials. You can configure credentials by running "aws configure".
   ```

   Instead of generating Access and Secret keys we will make use of IAM to permit the EC2 Instance
   (and apps running on it) access to S3. Curl the metadata endpoint and notice the lack of 'iam'.

   ```bash
     $ curl --silent http://169.254.169.254/latest/meta-data | grep iam
   ```

1. Create IAM resources (see [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/iam-roles-for-amazon-ec2.html#working-with-iam-roles)
   for a reference) by running Terraform in `iam` directory.

1. Enable the instance profile on EC2 by running Terraform with a TF_VAR.
   Curl metadata endpoint again and notice that 'iam' path is now present.

   ```bash
     $ TF_VAR_instance_profile_enabled=1 terraform apply
   ```

1. Retry accessing S3. It will hang, since there's no Internet access.

   ```bash
     $ ssh -A -J ec2-user@${BASTION_PUBLIC_DNS} ec2-user@${PRIVATE_INSTANCE_IP}
     $ aws s3 ls
   ```

1. Enable VPC endpoints by running Terraform in `endpoint` directory.

   A VPC Endpoint for the S3 service in the used *AWS Region* will be created. It will also be
   associated with the private subnet route table which was created by the `vpc` configuration.
   Since the VPC Endpoint is used as a target in a route, it needs to be associated with IPs
   or IP ranges which it is a next hop for. In this case these are called [prefix lists](https://docs.aws.amazon.com/vpc/latest/userguide/managed-prefix-lists.html).

   A prefix list, in case of AWS-managed prefix lists, is a list of IP CIDRs associated with
   an AWS service. These prefix lists are *regional*, which means they differ between regions.

   Run `aws ec2 describe-prefix-lists` to view the prefix lists for the currently configured
   AWS region.

1. Retry accessing S3.

   ```bash
     $ ssh -A -J ec2-user@${BASTION_PUBLIC_DNS} ec2-user@${PRIVATE_INSTANCE_IP}
     $ aws s3 ls
   ```

   Still hangs! What can be the issue?

   <details>
      <summary>Show answer</summary>
      <p>

    The troubleshooting [doc](https://aws.amazon.com/premiumsupport/knowledge-center/connect-s3-vpc-endpoint/)
    contains a hint to configure the `aws` CLI with the correct region:

    ```bash
      aws --region <region> s3 ls
    ```

    The command should return a list of buckets you have created in your account.

    NOTE: You might be able to list all the buckets in your account, but you will only be able to
    access and use buckets in the same region. Gateway VPC Endpoints (S3 and DynamoDB will only
    support services in the same region).

    </details>

1. Once done labbing - destroy all resources with Terraform.

   ```bash
     $ terraform destroy
   ```

## Review

- What is a VPC endpoint?

    <details>
      <summary>Show answer</summary>
      <p>

    A VPC endpoint is a virtual device which enable private connections between a VPC and AWS
    services. Instances in the VPC do not require public IP addresses to communicate with the
    service behind the endpoint. The traffic does not leave the Amazon network.

    </details>
