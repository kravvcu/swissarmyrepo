local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

{
  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/s3"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  's3.tf.json': {
    resource: {
      aws_s3_bucket: {
        bucket: {
          bucket: "${var.bucket_name}",
          acl: "private"
        }
      }
    }
  },

  'variables.tf.json': {
    variable: {
      bucket_name: {
        type: "string",
      }
    }
  }
}
