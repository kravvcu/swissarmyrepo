local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

local igw = import "igw.libsonnet";
local route = import "route.libsonnet";
local route_table = import "route_table.libsonnet";
local route_table_association = import "route_table_association.libsonnet";

{
  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/routing"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'data_sources.tf.json': {
    data: {
      terraform_remote_state: {
        vpc: env.EnvTerraformS3RemoteState {
          key_suffix:: '/vpc'
        },
        nat: env.EnvTerraformS3RemoteState {
          key_suffix:: '/nat'
        },
      },
    },
  },

  'routing.tf.json': {
    resource: {
      aws_internet_gateway: {
        igw: igw.InternetGateway {
          name:: env.resource_name_prefix + "-igw",
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",
        }
      },
      aws_route_table: {
        rt: route_table.RouteTable {
          name:: env.resource_name_prefix + "-rt",
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",
          route: [
            route.Route {
              cidr_block: "0.0.0.0/0",
              gateway_id: "${aws_internet_gateway.igw.id}"
            }
          ]
        },
        private: route_table.RouteTable {
          count: "${var.nat_enabled ? 1 : 0}",
          name:: env.resource_name_prefix + "-private-subnet-rt",
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",
          route: [
            route.Route {
              cidr_block: "0.0.0.0/0",
              nat_gateway_id: "${data.terraform_remote_state.nat.outputs.nat_gateway_id}",
            }
          ]
        }
      },
      aws_route_table_association: {
        rta: route_table_association.RouteTableAssociation {
          route_table_id: "${aws_route_table.rt.id}",
          subnet_id: "${data.terraform_remote_state.vpc.outputs.public_subnet_id}"
        },
        private_rta: route_table_association.RouteTableAssociation {
          count: "${var.nat_enabled ? 1 : 0}",
          route_table_id: "${aws_route_table.private.0.id}",
          subnet_id: "${data.terraform_remote_state.vpc.outputs.private_subnet_id}"
        },
      },
    }
  },

  'variables.tf.json': {
    variable: {
      nat_enabled: {
        type: "bool",
        default: false,
      }
    }
  }
}
