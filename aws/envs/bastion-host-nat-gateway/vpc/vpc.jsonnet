local provider = import "aws_provider.libsonnet";

local vpc = import "vpc.libsonnet";
local env = import "../env.jsonnet";

{
  vpc_cidr_block:: "10.6.0.0/16",
  public_subnet_cidr_block:: "10.6.15.0/24",
  private_subnet_cidr_block:: "10.6.16.0/24",

  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/vpc"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'aws_vpc.tf.json': {
    resource: {
      aws_vpc: {
        main: vpc.AWSVpc {
          name: env.resource_name_prefix,
          cidr_block: $.vpc_cidr_block,
          enable_dns_hostnames: true,
        }
      },
      aws_subnet: {
        public: vpc.AWSSubnet {
          name: env.resource_name_prefix + '-public',
          vpc_id: "${aws_vpc.main.id}",
          cidr_block: $.public_subnet_cidr_block,
        },
        private: vpc.AWSSubnet {
          name: env.resource_name_prefix + '-private',
          vpc_id: "${aws_vpc.main.id}",
          cidr_block: $.private_subnet_cidr_block,
        },
      },
    }
  },

  'outputs.tf.json': {
    output: {
      vpc_id: {
        value: "${aws_vpc.main.id}",
      },

      public_subnet_id: {
        value: "${aws_subnet.public.id}",
      },

      private_subnet_id: {
        value: "${aws_subnet.private.id}",
      }
    }
  }
}
