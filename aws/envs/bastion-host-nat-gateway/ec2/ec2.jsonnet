local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

local instance = import "ec2_instance.libsonnet";
local security_group = import "security_group.libsonnet";

{
  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/ec2"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'data_sources.tf.json': {
    data: {
      http: {
        own_ip: {
          url: "http://ipinfo.io/ip",
        }
      },
      aws_ami: {
        bastion: instance.AWSAMI {
          owner:: "679593333241", # Continuous
          name:: "continuous-bastion-1.0.2-1e231703-9044-4c58-b471-4b6345daf4a4-ami-0eb177f6a414935d8.4",
        },
        ubuntu: instance.AWSAMI,
      },
      terraform_remote_state: {
        vpc: env.EnvTerraformS3RemoteState {
          key_suffix:: '/vpc'
        },
      },
    },
  },

  'ec2.tf.json': {
    resource: {
      aws_key_pair: {
        kp: {
          key_name_prefix: env.resource_name_prefix,
          public_key: '${file("~/.ssh/id_rsa.pub")}',
        },
      },
      aws_security_group: {
        permit_self: {
          name_prefix: env.resource_name_prefix + "-permit-own-ip-sg",
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",

          ingress: [
            security_group.SecurityGroupRule {
              description: "Permit inbound SSH traffic",
              from_port: "22",
              to_port: "22",
              protocol: "tcp",
              cidr_blocks: ["${chomp(data.http.own_ip.body)}/32"],
            }
          ],

          egress: [
            security_group.SecurityGroupRule {
              description: "Permit all outbound traffic",
              from_port: "0",
              to_port: "0",
              protocol: "-1",
              cidr_blocks: ["0.0.0.0/0"],
            }
          ]
        },
        permit_ssh: {
          name_prefix: env.resource_name_prefix + "-permit-ssh-sg",
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",

          ingress: [
            security_group.SecurityGroupRule {
              description: "Permit inbound SSH traffic",
              from_port: "22",
              to_port: "22",
              protocol: "tcp",
              security_groups: ["${aws_security_group.permit_self.id}"]
            }
          ],

          egress: [
            security_group.SecurityGroupRule {
              description: "Permit all outbound traffic",
              from_port: "0",
              to_port: "0",
              protocol: "-1",
              cidr_blocks: ["0.0.0.0/0"],
            }
          ]
        }
      },
      aws_instance: {
        bastion: instance.AWSInstance {
          name: env.resource_name_prefix + '-bastion',
          ami: "${data.aws_ami.bastion.id}",
          key_name: '${aws_key_pair.kp.key_name}',
          subnet_id: "${data.terraform_remote_state.vpc.outputs.public_subnet_id}",
          vpc_security_group_ids: [
            "${aws_security_group.permit_self.id}"
          ],
          associate_public_ip_address: true,
        },
        private: instance.AWSInstance {
          name: env.resource_name_prefix + '-private-ubuntu',
          ami: "${data.aws_ami.ubuntu.id}",
          key_name: '${aws_key_pair.kp.key_name}',
          subnet_id: "${data.terraform_remote_state.vpc.outputs.private_subnet_id}",
          vpc_security_group_ids: [
            "${aws_security_group.permit_ssh.id}"
          ],
        }
      }
    }
  }
}
