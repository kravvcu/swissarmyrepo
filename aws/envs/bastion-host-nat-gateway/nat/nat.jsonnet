local provider = import "aws_provider.libsonnet";

local env = import "../env.jsonnet";

{

  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/nat"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'data_sources.tf.json': {
    data: {
      terraform_remote_state: {
        vpc: env.EnvTerraformS3RemoteState {
          key_suffix:: '/vpc'
        },
      },
    },
  },

  'nat.tf.json': {
    resource: {
      aws_eip: {
        nat: {
          vpc: true ,
          tags: {
            Name: env.resource_name_prefix + "-eip",
          }
        }
      },

      aws_nat_gateway: {
        nat: {
          allocation_id: "${aws_eip.nat.id}",
          subnet_id: "${data.terraform_remote_state.vpc.outputs.public_subnet_id}"
        }
      }
    }
  },

  'outputs.tf.json': {
    output: {
      nat_gateway_id: {
        value: "${aws_nat_gateway.nat.id}",
      },
    }
  }
}
