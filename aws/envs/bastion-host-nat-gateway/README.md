# Bastion Host & NAT Gateway

In this scenario we will set up a VPC with two subnets: a public one and a private one.
The public subnet will be host to a Bastion Host EC2 Instance. The private one will host
an EC2 Instance to which we will connect using the Bastion Host.

Then we will create a NAT Gateway to allow the private instance access to services
on the Internet (e.g. APT repositories).

## Documentation:

- https://aws.amazon.com/blogs/security/securely-connect-to-linux-instances-running-in-a-private-amazon-vpc/
- https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html

## Exercises

1. Create a VPC by running Terraform in the `vpc` directory.

   This will create two subnets:
   - a public one, which the bastion host will use
   - a private one, which will be used by our private EC2 Instance, to which we will not have
     direct access to

1. Configure routing by running Terraform in the `routing` directory.
1. Create the EC2 Instances by running Terraform in the `ec2` directory.
1. SSH to the `private` EC2 Instance using the bastion host.

    <details>
      <summary>Show answer</summary>
      <p>

      ``` bash
        $ PRIVATE_INSTANCE_IP=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=bastion-host-private-ubuntu" --query 'Reservations[*].Instances[*].{Name:PrivateIpAddress}' --output text)
        $ BASTION_PUBLIC_DNS=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=bastion-host-bastion" --query 'Reservations[*].Instances[*].{Name:PublicDnsName}' --output text)
        $ ssh -A -J ec2-user@${BASTION_PUBLIC_DNS} ubuntu@${PRIVATE_INSTANCE_IP}
      ```

      </p>
    </details>

    Execute `ping google.com` or `apt update` and notice that connections to the services will fail.
    This is because:

    - The private instance doesn't have an interface with a publicly reachable IP address.
    - The route associated with the subnet the instance has an interface in does not have
      a default route for routing traffic to the Internet.

    We will create a NAT Gateway and configure it as a next hop for the private subnet.

    *CAUTION: Hourly usage and data processing rates apply for a deployed NAT Gateway.*

1. Create the NAT Gateway - run Terraform in `nat` directory.
1. Re-run Terraform in `routing` to create the route table with the necessary default route targeting
   the created NAT Gateway and associate it with the private subnet:

   ```bash
     $ TF_VAR_nat_enabled=1 terraform apply
   ```

## Review

- What is the difference between a public subnet and a private subnet?

    <details>
      <summary>Show answer</summary>
      <p>

    A public subnet is one which has a route table associated that contains a route to an Internet
    gateway.

    </details>

- What is a Bastion Host?

    <details>
      <summary>Show answer</summary>
      <p>

    A Bastion Host is a server with a public IP address whose purpose is to provide access to the
    servers on the internal (private) network. It's usually security-crafted to limit the attack
    vector.

    </details>

- What is a NAT Gateway?

    <details>
      <summary>Show answer</summary>
      <p>

    A NAT Gateway is a managed service providing access to the Internet to EC2 Instances on a
    private subnet. It's deployed in a public subnet and configured as a default route target
    in the private subnet's route table.

    </details>

- What is the difference between an Internet Gateway and a NAT gateway?

    <details>
      <summary>Show answer</summary>
      <p>

    An Internet Gateway provides access to the Internet for EC2 Instances with assigned public IP
    addresses in a subnet which has the Internet Gateway configured as a next hop for the default
    route in that subnet's routing table. The resources on the Internet can also reach the EC2
    Instances.

    A NAT Gateway provides Internet access for EC2 Instances without public IP addresses assigned
    to them. The Gateway is deployed in a public subnet (one with an Internet Gateway) and is
    configured as the next hop for the default route of the subnet in which the EC2 Instances
    are deployed in. The resources on the Internet cannot reach the EC2 Instances.

    </details>
