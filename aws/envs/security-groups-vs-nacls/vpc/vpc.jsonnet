local provider = import "aws_provider.libsonnet";

local vpc = import "vpc.libsonnet";
local env = import "../env.jsonnet";

{
  vpc_cidr_block:: "10.5.0.0/16",
  subnet_cidr_block:: "10.5.5.0/27",

  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/vpc"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'aws_vpc.tf.json': {
    resource: {
      aws_vpc: {
        main: vpc.AWSVpc {
          name: env.resource_name_prefix,
          cidr_block: $.vpc_cidr_block,
          enable_dns_hostnames: true,
        }
      },
      aws_subnet: {
        subnet: vpc.AWSSubnet {
          name: env.resource_name_prefix,
          vpc_id: "${aws_vpc.main.id}",
          cidr_block: $.subnet_cidr_block,
        }
      }
    }
  },

  'outputs.tf.json': {
    output: {
      vpc_id: {
        value: "${aws_vpc.main.id}",
      },

      subnet_id: {
        value: "${aws_subnet.subnet.id}",
      }
    }
  }
}
