local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

local nacl = import "nacl.libsonnet";

{
  cidr_block:: 'Replace with own IP in CIDR format to permit SSH',

  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/nacl"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'data_sources.tf.json': {
    data: {
      terraform_remote_state: {
        vpc: env.EnvTerraformS3RemoteState {
          key_suffix:: '/vpc'
        },
      },
    },
  },

  'nacl.tf.json': {
    resource: {
      aws_network_acl: {
        permit_self: {
          vpc_id: "${data.terraform_remote_state.vpc.outputs.vpc_id}",
          subnet_ids: [
            "${data.terraform_remote_state.vpc.outputs.subnet_id}"
          ],

          ingress: [
            nacl.NACLRule {
              rule_no: 1000,
              from_port: "22",
              to_port: "22",
              protocol: "tcp",
              action: "allow",
              cidr_block: $.cidr_block,
            },
          ],

          tags: {
            Name: env.resource_name_prefix + "-acl",
          }
        },
      },
    }
  }
}
