# Security Groups vs Network Access Control Lists

The [VM in default VPC](./envs/vm-in-default-vpc/README.md) and [VM in custom VPC](./envs/vm-in-custom-vpc/README.md)
showed that proper Security Group rules are needed to allow traffic into and out of a VM. This
section shows how NACLs differ from Security Groups and how the two work together.

## Documentation:

- https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html
- https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Security.html#VPC_Security_Comparison

## Exercises

1. Create a VPC by running Terraform in the `vpc` directory.
1. Create a VM in the VPC by running Terraform in the `ec2` directory.

    - Adjust the `cidr_blocks` value in `ec2/ec2.jsonnet`.
    - Regenerate the Terraform resource definitions and run Terraform:

      ```bash
         $ jsonnet -J ../../../lib --multi . ec2.jsonnet
         $ teraform apply
      ```

1. Configure routing by running Terraform in the `routing` directory.

    At this point the setup is the same as at the end of the [VM in custom VPC](./envs/vm-in-custom-vpc/README.md)
    scenario - it's possible to SSH into the VM using Instance Connect CLI.

1. Create a Network Access Control List to add yet another layer of security.

    - Adjust the `cidr_block` value in `nacl/nacl.jsonnet`.
    - Re-generate the Terraform resource definitions and run Terraform:

    ```bash
      $ jsonnet -J ../../../lib --multi . nacl.jsonnet
      $ terraform apply
    ```

1. Although the NACL permits access to port `22` from our IP address, we can no longer SSH
   to our VM. Why?

    <details>
      <summary>Show answer</summary>
      <p>

      We did not permit outbound traffic which would allow for return traffic from the SSH server
      to return to us. This is one of the differences between NACLs and Security Groups:

      - Security Groups are *stateful* - return traffic is automatically allowed
      - NACLs are *stateless* - traffic needs to be explicitly allowed in each direction

     To permit return traffic to reach us, add the below `egress` configuration to the `network_acl`
     resource in `nacl/nacl.jsonnet`:

     ```bash
       egress: [
         nacl.NACLRule {
           rule_no: 1000,
           from_port: "1024",
           to_port: "65535",
           protocol: "tcp",
           action: "allow",
           cidr_block: $.cidr_block,
         }
       ],
     ```

      ```bash
        $ cd nacl
        $ # edit nacl.jsonnet
        $ jsonnet -J ../../../../lib --multi . nacl.jsonnet
        $ terraform apply
      ```

      The egress rule specifies a range of [ephemeral ports][ephemeral-ports].

      </p>
    </details>

1. DNS resolution works, although we did not explicitly permit outbound DNS (UDP port 53) traffic.
   Why?

    <details>
      <summary>Show answer</summary>
      <p>

      Some services and IP addresses are not subject to Security Group or NACL filtering

      ```bash
        Amazon Security Groups and Network ACLs don't filter traffic to or from link-local addresses
        (169.254.0.0/16) or AWS reserved IPv4 addresses (these are the first four IPv4 addresses of the
        subnet, including the Amazon DNS server address for the VPC). Similarly, flow logs do not capture
        IP traffic to or from these addresses. These addresses support the following:

        - Domain Name Services (DNS)
        - Dynamic Host Configuration Protocol (DHCP)
        - Amazon EC2 instance metadata
        - Key Management Server (KMS) - license management for Windows instances
        - Routing in the subnet
      ```

      </p>
    </details>

1. `apt update` is not working. Configure the necessary NACL rule(s) to get it fixed.

    <details>
      <summary>Show answer</summary>
      <p>

      Add an `egress` rule which will permit traffic to port `80` on the Internet:

      ```bash
        nacl.NACLRule {
          rule_no: 900,
          from_port: "80",
          to_port: "80",
          protocol: "tcp",
          action: "allow",
          cidr_block: "0.0.0.0/0",
        }
      ```

      Correspondingly, returning `ingress` traffic needs to be allowed:

      ```bash
        nacl.NACLRule {
          rule_no: 900,
          from_port: "1024",
          to_port: "65535",
          protocol: "tcp",
          action: "allow",
          cidr_block: "0.0.0.0/0",
        },
      ```

      </p>
    </details>

1. Will SSH connection to the EC2 Instance be possible if we disallow SSH access on the Security
   Group level? Will NACL take precedence over Security Group? Edit the `aws_security_group`
   resource in `ec2/ec2.jsonnet` by setting:

     ```bash
       ingress = [],
     ```

     ```bash
         $ jsonnet -J ../../../lib --multi . ec2.jsonnet
         $ teraform apply
     ```

    <details>
      <summary>Show answer</summary>
      <p>

      NACLs operate on the subnet level, while Security Groups apply to particular instances
      which they are assigned to. These security solutions work together to form a flexible firewall.

      In the given scenario, SSH traffic will not reach the EC2 Instance. It will pass through
      the NACL but get blocked closer to the instance, on the Security Group layer.
      </p>
    </details>

## Review

- Each VPC can have any number of NACLs defined, but a VPC subnet can be associated with only one.
- When a VPC is created, there is a default NACL created for it. Each new subnet created in that VPC
  is associated with the default NACL initially. The default NACL permits all inbound and outbound
  traffic.
- NACL rules are stateless, which means permitting `egress` port `80` or `443` traffic to access
  HTTP/S alone will not suffice. Returning `ingress` traffic needs to also be explicitly permitted
  to the port which the traffic originated from (see [ephemeral ports][ephemeral-ports]).
- Security Groups operate on the instance level they are assigned to. Network ACLs operate on the
  subnet level - they apply to all instances in that subnet.
- For a list of traffic types not filtered by NACLs (or Security Groups) see [here](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Security.html#VPC_Security_Comparison).

[ephemeral-ports]:  https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html#nacl-ephemeral-ports
