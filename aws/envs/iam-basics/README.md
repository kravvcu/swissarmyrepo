# IAM Basics

In this scenario we will cover the basic aspects of AWS IAM. We will create an IAM user
and provide him only with a single permission to list the S3 buckets in our account.

## Documentation:

- https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction_identity-management.html
- https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction_access-management.html
- https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html
- https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_identity-vs-resource.html
- https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles.html

## Exercises

1. Create an IAM user by running Terraform in `user` directory. The created user's
   access key ID and secret key will be available in the outputs.
   Create a new AWS CLI profile and switch to using it for labbing purposes in a separate
   window.

   ```bash
       $ terrraform apply
       $ aws --profile iam-basics configure

       # pass in the Terraform-output values

       # execute in a separate window
       $ export AWS_PROFILE=iam-basics
   ```

  NOTE: The following steps are prefixed with either [DEFAULT] or [IAM-BASICS] to specify to
  execute the step using the `default` and `iam-basics` profile accordingly.

1. [DEFAULT] Create an S3 bucket by running Terraform in `s3` directory. Pass in the name of your
   bucket.

  ```bash
  $ TF_VAR_bucket_name='chosen-bucket-name' terraform apply
  ```

1. [IAM-BASICS] Try to list S3 buckets.

  ```bash
  $ aws s3 ls
  ```

  We created the IAM user, but we did not give it any permissions so the API call fails.

1. [DEFAULT] Assign the necessary permission to the IAM user to list the buckets by running
   Terraform in `iam-list-buckets` directory.

1. [IAM-BASICS] Try to list S3 buckets again. This time the API call executes successfully.

  ```bash
  $ aws s3 ls
  ```

1. [IAM-BASICS] Try to list S3 objects in the bucket created in this exercise.

  ```bash
  $ aws s3 ls s3://chosen-bucket-name
  ```

  Once again, the API call fails. We did, in fact, only permit the user to list what buckets are
  there, but we didn't allow it to take a look *into* any bucket or create any objects.

1. [DEFAULT] Permit the user to list and create objects in the created S3 bucket.

    Tip: You can use the AWS console's Policy visual editor to see what permissions
    are available for any service and what action do they actually permit a user or service to
    perform (open a browser and navigate to the AWS Console -> IAM -> Policies -> Create policy).

    <details>
      <summary>Show answer</summary>
      <p>

      You can either:
      - modify the Policy in `iam-list-buckets`, re-generate the Terraform files using jsonnet and
        apply the configuration
      - create an entirely new configuration e.g. `iam-list-and-create-bucket-objects`

      The standalone policy which fulfills the requirements:

      ```json
      {
          "Version": "2012-10-17",
          "Statement": [
            {
                "Sid": "AllowObjectListAndWrite",
                "Effect": "Allow",
                "Action": [
                    "s3:ListBucket",
                    "s3:PutObject",
                ],
                "Resource": [
                    "arn:aws:s3:::iam-basics",
                    "arn:aws:s3:::iam-basics/*"
                ]
            }
          ]
      }
      ```

      </p>
    </details>

1. [IAM-BASICS] Verify that the policy works as intended.

  ```bash
  $ aws s3 ls s3://chosen-bucket-name
  $ touch file
  $ aws s3 cp file s3://chosen-bucket-name
  ```

1. [DEFAULT] `terraform destroy` the created Policies (`iam-list-buckets` configuration and the one allowing
  to list and create bucket objects).

1. [DEFAULT] The Policy created in `iam-list-buckets` is an `identity-based` policy. These are attached to
  an AWS Identity. There are also `resource-based` policies that are attached to a resource.
  Create a `resource-based` policy which will permit the user to create (but not list) bucket
  objects and attach it to the created S3 bucket.

    <details>
      <summary>Show answer</summary>
      <p>

      An S3 `resource-based` policy is called a `bucket policy`. This policy is already implemented
      as part of the `s3` configuration. To enable it, run Terraform passing in your AWS account ID:

      ```bash
      $ TF_VAR_bucket_name='chosen-bucket-name' TF_VAR_aws_account_id=your-id terraform apply
      ```

    </details>

1. [IAM-BASICS] Verify that the policy works as intended.

  ```bash
  $ touch file
  $ aws s3 cp file s3://chosen-bucket-name/newfile

  # should fail
  $ aws s3 ls s3://chosen-bucket-name
  ```

1. `terraform destroy` all configurations to cleanup.

## Review

- What is an IAM role?

    <details>
      <summary>Show answer</summary>
      <p>

    An IAM role is an AWS IAM Identity which, like an IAM user, can be assigned IAM Policies.
    It is not associated with a single person, instead is meant to be assumable by users,
    applications or services.

    </details>

- How do identity-based policies differ from resource-based ones?

    <details>
      <summary>Show answer</summary>
      <p>

    An identity-based policy is attached to an AWS Identity (user, group, role) and it manages
    permissions of that Identity to AWS services (in other words, it answers the question
    `what does the Identity allowed or denied to do?`).

    A resource-based policy is attached to an AWS resource. It manages access to the resource
    (in other words, it answers the question `who is allowed or denied to do what on the resource?`).

    </details>

- It's possible to determine whether a policy is being used in the `Policy usage` tab of a given
  policy in AWS management console.
