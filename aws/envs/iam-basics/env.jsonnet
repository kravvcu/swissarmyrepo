local common = import "../../common.jsonnet";

{
  environment:: "iam-basics",
  resource_name_prefix:: $.environment,
  region:: "eu-central-1",

  EnvTerraformBackend:: common.TerraformS3Backend {
    local backend = self,
    tfstate_suffix:: error "Terraform state suffix is required",

    s3_tfstate_prefix: "envs/" + $.environment + backend.tfstate_suffix
  },

  EnvTerraformS3RemoteState:: common.TerraformS3RemoteState {
    local remote_state = self,

    key_suffix:: error 'Remote state suffix is required',

    key_prefix: 'envs/' + $.environment + remote_state.key_suffix,
  }
}
