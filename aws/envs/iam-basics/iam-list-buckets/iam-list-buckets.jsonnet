local provider = import "aws_provider.libsonnet";

local iam = import "iam.libsonnet";
local env = import "../env.jsonnet";

local iam_policy = {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowBucketListing",
      "Effect": "Allow",
      "Action": "s3:ListAllMyBuckets",
      "Resource": "*",
    }
  ]
};

{
  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/iam-list-buckets"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'data_sources.tf.json': {
    data: {
      terraform_remote_state: {
        user: env.EnvTerraformS3RemoteState {
          key_suffix:: '/user'
        },
      },
    },
  },

  'iam.tf.json': {
    resource: {
      aws_iam_policy: {
        s3_list_buckets: iam.IAMPolicy {
          name_prefix: env.resource_name_prefix + "-s3-list-buckets",
          policy: std.manifestJson(iam_policy)
        }
      },
      aws_iam_user_policy_attachment: {
        permit_iam_user_list_buckets: {
          user: "${data.terraform_remote_state.user.outputs.name}",
          policy_arn: "${aws_iam_policy.s3_list_buckets.arn}"
        }
      }
    },
  },
}
