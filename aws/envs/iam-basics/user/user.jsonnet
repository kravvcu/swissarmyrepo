local provider = import "aws_provider.libsonnet";

local iam = import "iam.libsonnet";
local env = import "../env.jsonnet";

{
  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/user"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'iam.tf.json': {
    resource: {
      aws_iam_user: {
        user: iam.IAMUser {
          name: env.resource_name_prefix + '-user'
        }
      },
      aws_iam_access_key: {
        key: iam.IAMAccessKey {
          user: "${aws_iam_user.user.name}"
        }
      }
    },
  },

  'outputs.tf.json': {
    output: {
      access_key_id: {
        value: "${aws_iam_access_key.key.id}"
      },
      secret_access_key: {
        value: "${aws_iam_access_key.key.secret}"
      },
      name: {
        value: "${aws_iam_user.user.name}",
      }
    }
  }
}
