local provider = import "aws_provider.libsonnet";
local env = import "../env.jsonnet";

local bucket_policy = {
  "Version": "2012-10-17",
  "Id": "PolicyIdIAMBasicsExercise",
  "Statement": [
    {
      "Sid": "StatementId12345",
      "Effect": "Allow",
      "Action": "s3:PutObject",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${var.aws_account_id}:user/${data.terraform_remote_state.user.outputs.name}"
        ]
      },
      "Resource": "arn:aws:s3:::${var.bucket_name}/*",
    }
  ]
};

{
  'terraform.tf.json': env.EnvTerraformBackend {
    tfstate_suffix: "/s3"
  },

  'providers.tf.json': {
    provider: provider.AWSProvider {
      region: env.region
    }
  },

  'data_sources.tf.json': {
    data: {
      terraform_remote_state: {
        user: env.EnvTerraformS3RemoteState {
          key_suffix:: '/user'
        },
      },
    },
  },

  's3.tf.json': {
    resource: {
      aws_s3_bucket: {
        bucket: {
          bucket: "${var.bucket_name}",
          acl: "private"
        }
      },
      aws_s3_bucket_policy: {
        allow_user_bucket_object_creation: {
          count: '${var.aws_account_id != "" ? 1 : 0}',
          bucket: "${aws_s3_bucket.bucket.id}",
          policy: std.manifestJson(bucket_policy)
        },
      },
    },
  },

  'variables.tf.json': {
    variable: {
      bucket_name: {
        type: "string",
      },
      aws_account_id: {
        type: "string",
        default: "",
      }
    }
  }
}
