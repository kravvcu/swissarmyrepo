{
  IAMRole:: {
    name_prefix: error "AWS IAM role 'name_prefix' is required",

    assume_role_policy: error "AWS IAM role 'assume_role_policy' is required",
  },

  IAMPolicy:: {
    name_prefix: error "AWS IAM policy 'name_prefix' is required",
    policy: error "AWS IAM policy 'policy' is required",
  },

  IAMRolePolicy:: {
    name_prefix: error "AWS IAM role policy 'name_prefix' is required",
    role: error "AWS IAM role policy 'role' is required",
    policy: error "AWS IAM role policy 'policy' is required",
  },

  IAMInstanceProfile:: {
    name_prefix: error "AWS IAM instance profile 'name_prefix' is required",
    role: error "AWS IAM instance profile 'role' is required",
  },

  IAMUser:: {
    name: error "AWS IAM user 'name' is required",
  },

  IAMAccessKey:: {
    user: error "AWS IAM Access Key 'user' is required",
  }
}
