{
  Route:: {
    cidr_block: "",
    ipv6_cidr_block: "",

    egress_only_gateway_id: "",
    gateway_id: "",
    instance_id: "",
    nat_gateway_id: "",
    network_interface_id: "",
    transit_gateway_id: "",
    vpc_peering_connection_id: "",
  },
}
