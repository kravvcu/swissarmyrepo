{
  TerraformRemoteState:: {
    local remote = self,

    backend: error 'Remote state backend is required',
    bucket:: error 'Remote state bucket is required',
    region:: error 'Remote state region is required',
    key_prefix:: error 'Remote state key prefix is required',

    config: {
      bucket: remote.bucket,
      key: remote.key_prefix + '/terraform.tfstate',
      region: remote.region,
    }
  }
}
