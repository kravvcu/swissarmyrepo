{
  SecurityGroupRule:: {
    local sg_rule = self,

    description: error "Security Group description is required",
    from_port: error "Security Group from_port is required",
    to_port: error "Security Group to_port is required",
    protocol: error "Security Group protocol is required",

    cidr_blocks: [],
    ipv6_cidr_blocks: [],
    'self': false,
    prefix_list_ids: [],
    security_groups: [],
  },
}
