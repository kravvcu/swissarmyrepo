{
  AWSVpc:: {
    local aws_vpc = self,

    name:: error "Name for the AWS VPC is required",
    cidr_block: error "CIDR block for the AWS VPC is required",

    tags: {
      Name: aws_vpc.name,
    }
  },

  AWSSubnet:: {
    local aws_subnet = self,

    name:: error "AWS subnet name is required",
    vpc_id: error "AWS subnet vpc_id is required",
    cidr_block: error "AWS subnet cidr_block is required",

    tags: {
      Name: aws_subnet.name,
    }
  },
}
