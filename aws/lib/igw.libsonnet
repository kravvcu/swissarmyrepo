{
  InternetGateway:: {
    local igw = self,

    name:: error "Name for the Internet Gateway is required",
    vpc_id: error "VPC ID for the Internet Gateway is required",

    tags: {
      Name: igw.name
    }
  },
}
