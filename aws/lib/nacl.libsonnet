{
  NACLRule:: {
    local nacl = self,

    rule_no: error "NACL rule rule_no is required",
    protocol: error "NACL rule protocol is required",
    action: error "NACL rule action is required",

    cidr_block: "",
    ipv6_cidr_block: "",
    icmp_code: 0,
    icmp_type: 0,
  },
}
