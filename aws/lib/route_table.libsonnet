{
  RouteTable:: {
    local rt = self,

    name:: error "Name for the Route Table is required",
    vpc_id: error "VPC ID for the Route Table is required",

    tags: {
      Name: rt.name
    },
  },
}
