{
  RouteTableAssociation:: {
    local rta = self,

    route_table_id: error "Route Table ID is needed for the association",
  },
}
