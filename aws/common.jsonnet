local terraform = import "terraform.libsonnet";
local remote_state = import "remote_state.libsonnet";

{
  TerraformS3Backend:: terraform.TerraformS3Backend {
    bucket_name:: 'kc-playground-aws-tfstate',
    region:: 'eu-central-1',
    dynamodb_table:: 'kc-playground-aws-tfstate-lock',
  },

  TerraformS3RemoteState:: remote_state.TerraformRemoteState {
    backend: 's3',
    bucket:: 'kc-playground-aws-tfstate',
    region:: 'eu-central-1'
  }
}
